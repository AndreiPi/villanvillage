﻿Shader "Custom/RoadShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _MainBump("Normal ",2D) = "white" {}
        _FadeTex("Fade mask",2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _BumpPower("bump power",Range(0.1,15))= 2
    }
    SubShader
    {
        Tags { "RenderType"="Transparent"
         "RenderQueue" = "Transparent" }
        
        

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha vertex:vert
        

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0
        
       

        sampler2D _MainTex;
        sampler2D _MainBump;
        sampler2D _FadeTex;
        float _BumpPower;
         half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_MainBump;
            float2 uv_FadeTex;
            float3 viewDir;
            float4 vertColor;
        };
        
         void vert (inout appdata_full v, out Input o) 
      {
          UNITY_INITIALIZE_OUTPUT(Input,o);
          o.vertColor = (0,0,0,1);
          //o.vertColor.rgb =v.color.rgb;
          //o.customcolor.r = v.vertex.r;
          half blue = abs(o.vertColor.z) < 0.7 ? 1-abs(o.vertColor.z) : 0 ;
          half red = abs(o.vertColor.x) < 0.7 ? 1-abs(o.vertColor.x) : 0 ;
          o.vertColor.a =v.color.a ;
      }

        
       

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            o.Alpha = IN.vertColor.a ;
            //o.Albedo = IN.vertColor.rgb;
            float3 up = float3(0.2,0,0.5);
            half rim = saturate(dot(normalize(IN.viewDir),o.Normal));
            //o.Emission = IN.viewDir;
           // o.Emission = IN.customcolor * pow(rim,1);
            //o.Emission = IN.vertColor.rgb;
            
            //Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Normal = UnpackNormal(tex2D(_MainBump,IN.uv_MainBump) )*_BumpPower;
            o.Normal *= float3(_BumpPower,_BumpPower,1);
            
        }
        ENDCG
    }
    FallBack "Diffuse"
}
