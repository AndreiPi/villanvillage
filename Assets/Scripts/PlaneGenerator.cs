﻿using System.Collections;
using System.Collections.Generic;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlaneGenerator : MonoBehaviour
{

    public GameObject DetectedPlanePrefab;
    
    private List<DetectedPlane> newPlanes = new List<DetectedPlane>();
    
    // Start is called before the first frame update
    void Start()
    {

        
    }
    

    // Update is called once per frame
    void Update()
    {
        if (Session.Status != SessionStatus.Tracking)
        {
            return;
        }
        Session.GetTrackables<DetectedPlane>(newPlanes,TrackableQueryFilter.New);
        for (int i = 0; i < newPlanes.Count; ++i)
        {
            
           var texture = ScreenCapture.CaptureScreenshotAsTexture();
           Color color =texture.GetPixel(texture.height/2, texture.width/2);

           //cleanup
           Destroy(texture);
           DetectedPlanePrefab.GetComponent<PlaneVisualizer>().planeColor=color;
            
            
            GameObject planeObject = Instantiate(DetectedPlanePrefab, Vector3.zero, Quaternion.identity, transform);
            planeObject.GetComponent<PlaneVisualizer>().Initialize(newPlanes[i]);
        }
    }
    
    
        
        
    IEnumerator RecordFrame()
    {
        yield return new WaitForEndOfFrame();
        var texture = ScreenCapture.CaptureScreenshotAsTexture();
        texture.GetPixel(0, 0);

        // cleanup
        Object.Destroy(texture);
    }
}
