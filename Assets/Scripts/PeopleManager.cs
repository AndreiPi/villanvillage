﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PeopleManager : MonoBehaviour
{
    public NavMeshSurface navMesh;

    public NavMeshAgent agent;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateNavMesh()
    {
        navMesh.BuildNavMesh();
    }
}
