using System;
using UnityEngine;

namespace DefaultNamespace
{
    [Serializable]
    public struct SpawnStruct
    {
        public SpawnObject index;
        public GameObject prefab;
    }
}