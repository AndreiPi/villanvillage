﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using DefaultNamespace;
using GoogleARCore;
using UnityEngine;

public class PersistanceObject : MonoBehaviour
{

    private SaveManager saveManager;

    public SaveManager SaveManager
    {
        get { return saveManager;}
        set
        {
            saveManager = value;
            saveManager.PersistanceObjects.Add(this);
        }

    }
    public PersistanceObjectTypes type;
    public bool needAnchor;
    public TrackableHit hit;

    /// <summary>
    /// Order ObjectType (vertices) needAnchor (hit position hit rotation)
    /// Position Rotation HasPersistancceParent parent ( same order)
    /// </summary>
    /// <param name="strWri"></param>
    public void Save(BinaryWriter strWri)
    {
        strWri.Write(type.ToString());
        if (type == PersistanceObjectTypes.Road)
        {
            Mesh mesh = GetComponent<MeshFilter>().mesh;
            strWri.Write(mesh.vertices.Length);
            for (int i = 0; i < mesh.vertices.Length; ++i)
            {
                strWri.Write(mesh.vertices[i].x);
                strWri.Write(mesh.vertices[i].y);
                strWri.Write(mesh.vertices[i].z);
            }
        }
        
        strWri.Write(needAnchor);
        strWri.Write(transform.position.x);
        strWri.Write(transform.position.y);
        strWri.Write(transform.position.z);
        strWri.Write(transform.rotation.x);
        strWri.Write(transform.rotation.y);
        strWri.Write(transform.rotation.z);
        strWri.Write(transform.rotation.w);
        if (transform.parent!=null && transform.parent.GetComponent<PersistanceObject>()!=null && needAnchor == false)
        {
            Debug.Log("Saving parent" + transform.parent.name);
            strWri.Write(true);
            //parent.Save(strWri);
        }
        else
        {
            strWri.Write(false);
        }
    }

    
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
