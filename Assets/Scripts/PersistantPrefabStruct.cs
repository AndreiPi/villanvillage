using System;
using UnityEngine;

namespace DefaultNamespace
{
    [Serializable]
    public struct PersistantPrefabStruct
    {
        public PersistanceObjectTypes index;
        public GameObject prefab;
    }
}