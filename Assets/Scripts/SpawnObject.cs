namespace DefaultNamespace
{
    public enum SpawnObject
    {
        Road,
        House,
        Store,
        Barracks,
        Noble,
        Church,
        Eraser,
        Projectile
    }
}