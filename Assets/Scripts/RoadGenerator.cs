﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using GoogleARCore;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class RoadGenerator : MonoBehaviour
{
    public SaveManager saveManager;
    public GameObject helpPrefab;
    
    public SpawnStruct[] SpawnStructs;
    public Camera sceneCamera;
    public float density;
    public float distance;
    public float minDistance;
    public float forcePower = 1;

    public SpawnObject choosenPrefab=SpawnObject.Road;
    private Dictionary<SpawnObject,GameObject> spawnPrefab;
    private string debugText = "nimic";
    private string roadTag = "road";
    public bool IsEditor;

    private bool generated;
    private GameObject proposedLand;
    private Vector2 initialTouchPos;
    
    private List<Vector3> vertices = new List<Vector3>();

    private float lastDrawTimeStamp;

    private Mesh mesh;

    private List<GameObject> helpPoints = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        spawnPrefab = new Dictionary<SpawnObject, GameObject>();
        foreach (var ss in SpawnStructs)
        {
            spawnPrefab.Add(ss.index,ss.prefab);
        }
    }

    private void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.fontSize = 30;
        GUI.Label(new Rect(10, 10, 150, 100), debugText, style);
    }

    // Update is called once per frame
    void Update()
    {
        if (choosenPrefab == SpawnObject.Eraser)
        {
            return;
        }
        if (IsEditor)
        {
            GenerateEditorLand();
        }
        else
        {
            GenerateLand();
        }
        
    }

    private bool TouchRoad(Touch touch)
    {
        if (Time.time - lastDrawTimeStamp < density)
        {
            return true;
        }

        if (touch.phase == TouchPhase.Ended)
        {
            generated = true;
        }
        
        if (touch.phase == TouchPhase.Began)
        {
            initialTouchPos = touch.position;
        }

        return false;
    }
    
    private bool TouchBuilding(Touch touch)
    {

        if (touch.phase != TouchPhase.Began)
        {
            return true;
        }

        return false;
    }

    private void GenerateLand()
    {
        if (spawnPrefab == null)
        {
            debugText = "Nu Avem prefab";
            return;
        }
        
        
        
        if (Input.touchCount < 1)
        {
            return;
        }
        Touch touch =  Input.GetTouch(0);
        
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        if (choosenPrefab == SpawnObject.Road)
        {
            if (TouchRoad(touch))
            {
                return;
            }
        }
        else
        {
            if (TouchBuilding(touch))
            {
                return;
            }
        }

        debugText = "Passed input";
        
        
        Vector3 mpos = touch.position;
//        if (initialTouchPos.x - Input.mousePosition.x > initialTouchPos.y - Input.mousePosition.y)
//        {
//            mpos.y = initialTouchPos.y;
//        }
//        else
//        {
//            mpos.x = initialTouchPos.x;
//        }


        // Raycast against the location the player touched to search for planes.
        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon;

        if (Frame.Raycast(mpos.x, mpos.y, raycastFilter, out hit))
        {
            if ((hit.Trackable is DetectedPlane) && Vector3.Dot(sceneCamera.transform.position - hit.Pose.position,
                    hit.Pose.rotation * Vector3.up) < 0)
            {
                Debug.Log("Hit at back of the current DetectedPlane");
            }
            else
            {
                Vector3 placePosition = hit.Pose.position + Vector3.up * 0.01f;

                if (choosenPrefab == SpawnObject.Road)
                {
                    DrawRoad(placePosition,hit);
                }
                else if (choosenPrefab == SpawnObject.Projectile)
                {
                    ShootProjectile((placePosition));
                }
                else
                {
                    DrawBuilding(placePosition,hit);
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < vertices.Count; ++i)
        {
            Gizmos.DrawSphere(vertices[i],0.05f);
        }
    }

    private void DrawRoad(Vector3 point, TrackableHit hit)
    {
        if (generated)
        {
            generated = false;
            if (vertices.Count > 4)
            {
                
                Anchor anchor=null;
                if (!IsEditor)
                {
                    anchor = hit.Trackable.CreateAnchor(hit.Pose);
                }
            
                Vector3 difference = vertices[0] - Vector3.zero;
                Debug.Log(vertices[0] + "  "+difference);
                for (int i = 0; i < vertices.Count; ++i)
                {
                    vertices[i] -= difference;
                    vertices[i] -= Vector3.up * vertices[i].y;
                }
//                        Vector3 difference = vertices[0] - anchor.transform.position;
//                        for (int i = 0; i < vertices.Count; ++i)
//                        {
//                            vertices[i] -= difference;
//                        }
                
                RoadPiece roadPiece = Instantiate(spawnPrefab[choosenPrefab], Vector3.zero, Quaternion.identity)
                    .GetComponent<RoadPiece>();
                
                
                Debug.Log(sceneCamera.transform.eulerAngles);
                roadPiece.transform.position += difference;
                //roadPiece.transform.Rotate(Vector3.up,sceneCamera.transform.eulerAngles.y);
                
                if (!IsEditor)
                {
                    roadPiece.transform.SetParent(anchor.transform,true);
                    //anchor.transform.localScale = Vector3.one*0.1f;
                }
                
                roadPiece.centerPoints.Clear();
                roadPiece.centerPoints.AddRange(vertices);
                roadPiece.GenerateStraightMesh();
                PersistanceObject po = roadPiece.GetComponent<PersistanceObject>();
                if (po != null)
                {
                    po.SaveManager = this.saveManager;
                    if (!IsEditor)
                    {
                        po.needAnchor = true;
                    }
                }

            }
            vertices.Clear();
                    
            debugText = "Avem Road";
            
            foreach (var p in helpPoints)
            {
                p.SetActive(false);
                Destroy(p);
            }
            helpPoints.Clear();
        }
        else
        {
            if (IsTooClose(point))
            {
                return;
            }

            AddQuadVertices(point);
            lastDrawTimeStamp = Time.time;
                    
            helpPoints.Add(Instantiate(helpPrefab,point,Quaternion.identity));
            debugText = "Help " +point;
        }
    }

    private bool InputRoadCondition()
    {
        if (Input.GetMouseButtonUp(0))
        {
            generated = true;
        }
        

        if (Input.GetMouseButtonDown(0))
        {
            initialTouchPos = Input.mousePosition;
            //Debug.Log("mouse input Down");
        }
        
        if (Time.time - lastDrawTimeStamp < density)
        {
            return true;
        }
        
        if (!Input.GetMouseButton(0) && generated==false)
        {
            return true;
        }

        return false;
    }
    
    private bool InputBuildingCondition()
    {

        if (!Input.GetMouseButtonDown(0))
        {
            return true;
        }

        return false;
    }

    private void GenerateEditorLand()
    {
        
        
        if (spawnPrefab == null )
        {
            debugText = "Nu Avem prefab";
            return;
        }
        
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        if (choosenPrefab == SpawnObject.Road)
        {
            if (InputRoadCondition())
            {
                return;
            }
        }
        else
        {
            if (InputBuildingCondition())
            {
                return;
            }
        }
        

        
        
        
        // Raycast against the location the player touched to search for planes.
        Vector3 mpos = Input.mousePosition;
//        if (initialTouchPos.x - Input.mousePosition.x > initialTouchPos.y - Input.mousePosition.y)
//        {
//            mpos.y = initialTouchPos.y;
//        }
//        else
//        {    
//            mpos.x = initialTouchPos.x;
//        }

       
        //Debug.Log(mpos);
        RaycastHit hit;
        Ray ray = sceneCamera.ScreenPointToRay(mpos);
        Vector3 point = Vector3.zero;
        Debug.DrawRay(ray.origin,ray.direction,Color.green);
        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log(hit.transform.name);
            if (!hit.transform.CompareTag("testfloor"))
            {
                return;
            }

            point = hit.point;
        }
        else
        {
            return;
        }
        //point.z = sceneCamera.transform.position.z + (point.y-sceneCamera.transform.position.y+2)*1.5f;
       // point.y = 9.4f;

        if (choosenPrefab == SpawnObject.Road)
        {
            DrawRoad(point,new TrackableHit());
        }
        else if (choosenPrefab == SpawnObject.Projectile)
        {
            ShootProjectile(point);
        }
        else
        {
            DrawBuilding(point, new TrackableHit());
        }


        
    }

    private void DrawBuilding(Vector3 point, TrackableHit hit)
    {
        if(RoadCollide(point))
        {
            return;
        }
        
        GameObject building =Instantiate(spawnPrefab[choosenPrefab], point, spawnPrefab[choosenPrefab].transform.rotation);
        PersistanceObject po = building.GetComponent<PersistanceObject>();
        if (po != null)
        {
            po.SaveManager = this.saveManager;
            if (!IsEditor)
            {
                po.needAnchor = true;
            }
        }
        if (!IsEditor)
        {
            building.transform.Rotate(new Vector3(0,0,hit.Pose.rotation.eulerAngles.z) );
            var anchor = hit.Trackable.CreateAnchor(hit.Pose);
            building.transform.SetParent(anchor.transform,true);
        }

    }

    private void ShootProjectile(Vector3 point)
    {
        GameObject ball =Instantiate(spawnPrefab[choosenPrefab], sceneCamera.transform.position, spawnPrefab[choosenPrefab].transform.rotation);
        Rigidbody ballRigid = ball.GetComponent<Rigidbody>();
        if (ballRigid != null)
        {
            ballRigid.AddForce((point-sceneCamera.transform.position)*forcePower);
        }
    }

    private bool IsTooClose(Vector3 point)
    {
        if (vertices.Count ==0)
        {
            return false;
        }
        Vector3 lastV = vertices[vertices.Count - 1];
        //lastV.x -= distance;
        //lastV.z += distance;
        return Vector3.Distance(point, lastV) < minDistance;
    }

    private void AddQuadVertices(Vector3 point)
    {
        vertices.Add(point);
        //vertices.Add(new Vector3(point.x-distance,point.y, point.z-distance));
        //vertices.Add(new Vector3(point.x-distance,point.y, point.z+distance));
        //vertices.Add(new Vector3(point.x+distance,point.y, point.z+distance));
        //vertices.Add(new Vector3(point.x+distance,point.y, point.z-distance));
        
    }

    private bool RoadCollide(Vector3 touchPosition)
    {
        Bounds prefabBox = spawnPrefab[choosenPrefab].GetComponent<MeshRenderer>().bounds;
        RaycastHit hit;
        Vector3 originTouch = touchPosition;
        //prefabBox.size -= Vector3.one *roadPrefab.transform.localScale.x;
        Vector3 origin = sceneCamera.transform.position;
        Debug.DrawLine(origin,touchPosition,Color.blue,5000);
        if (Physics.Raycast(origin,touchPosition - origin ,out hit))
        {
            Debug.Log("We hit");
            if (hit.transform.CompareTag(roadTag))
            {
                return true;
            }
        }

        touchPosition = originTouch;
        touchPosition.x += prefabBox.size.x / 2;
        Debug.DrawLine(origin,touchPosition,Color.blue,5000);
        if (Physics.Raycast(origin,touchPosition - origin ,out hit))
        {
            if (hit.transform.CompareTag(roadTag))
            {
                return true;
            }
        }

        touchPosition = originTouch;
        touchPosition.x -= prefabBox.size.x / 2;
        Debug.DrawLine(origin,touchPosition,Color.blue,5000);
        if (Physics.Raycast(origin,touchPosition - origin ,out hit))
        {
            if (hit.transform.CompareTag(roadTag))
            {
                return true;
            }
        }

        touchPosition = originTouch;
        touchPosition.z += prefabBox.size.y / 2;
        Debug.DrawLine(origin,touchPosition,Color.blue,5000);
        if (Physics.Raycast(origin,touchPosition - origin ,out hit))
        {
            if (hit.transform.CompareTag(roadTag))
            {
                return true;
            }
        }

        touchPosition = originTouch;
        touchPosition.z -= prefabBox.size.y / 2;
        Debug.DrawLine(origin,touchPosition,Color.blue,5000);
        if (Physics.Raycast(origin,touchPosition - origin ,out hit))
        {
            if (hit.transform.CompareTag(roadTag))
            {
                return true;
            }
        }

        return false;
    }
    
}