﻿using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class ObjectRemoval : MonoBehaviour
{

    public Camera camera;

    public bool IsEditor;
    public RoadGenerator roadGenerator;

    private void RemoveObject()
    {
        Vector3 target=Vector3.zero;
        if (IsEditor)
        {
            target = MouseInput();
        }
        if (!IsEditor)
        {
            target = TouchInput();
        }
        if (target.Equals( Vector3.negativeInfinity))
        {
            return;
        }

        Vector3 origin = camera.transform.position;
        
        target.z += 1;
        RaycastHit hit;
        Debug.Log( target);
        Vector3 point = camera.ScreenToWorldPoint(target);

        Debug.DrawRay(origin,(point - origin).normalized,Color.green,100.0f);
        if (Physics.Raycast(origin, (point - origin).normalized, out hit))
        {
            if (hit.transform.CompareTag("floor"))
            {
                return;
            }
            List<Transform> parents = new List<Transform>();
            Transform parent = hit.transform.parent;
            while (parent!= null && parent.transform.childCount == 1)
            {
                parents.Add(parent);
                parent = parent.transform.parent;
            } 
            hit.transform.gameObject.SetActive(false);
            Destroy(hit.transform.gameObject);
            foreach (var p in parents)
            {
                p.gameObject.SetActive(false);
                Destroy(p);
            }
            
        }
            
    }

    private Vector3 MouseInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            
            return Input.mousePosition;
        }

        return Vector3.negativeInfinity;
    }

    private Vector3 TouchInput()
    {
        if (Input.touchCount != 1)
        {
            return Vector3.negativeInfinity;
        }
        Touch touch = Input.GetTouch(0);
        if (touch.phase != TouchPhase.Began)
        {
            return Vector3.negativeInfinity;
        }

        return touch.position;

    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (roadGenerator.choosenPrefab != SpawnObject.Eraser)
        {
            return;
        }
        RemoveObject();
    }
}
