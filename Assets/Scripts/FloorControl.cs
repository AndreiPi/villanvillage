﻿using GoogleARCore;
using UnityEngine;

public class FloorControl : MonoBehaviour
{

    private bool isSet;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isSet)
        {
            SetFloor();
        }
    }

    private void SetFloor()
    {
        TrackableHit hit;
        TrackableHitFlags filter = TrackableHitFlags.PlaneWithinPolygon;
        if (Frame.Raycast(Screen.width / 2, Screen.height / 2, filter, out hit))
        {
            transform.position = hit.Pose.position;
            isSet = true;
        }
    }
}
