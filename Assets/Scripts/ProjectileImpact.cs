﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileImpact : MonoBehaviour
{
    public float forcePower = 500;
    public GameObject explosionEffect;
    public float radius = 10;
    private float timeAlive=0;
    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag != "floor")
        {
            Rigidbody otherRigid = other.transform.GetComponent<Rigidbody>();
            if (otherRigid != null)
            {
                GameObject explosion = Instantiate(explosionEffect, transform.position, transform.rotation);
                explosion.transform.localScale = transform.localScale;
                Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
                foreach (var c in colliders)
                {
                    Vector3 direction = c.transform.position - transform.position;
                    Rigidbody rc = c.GetComponent<Rigidbody>();
                    if (rc != null)
                    {
                        rc.AddExplosionForce(forcePower,transform.position,radius);
                    }
                
                }
            
            
                transform.gameObject.SetActive(false);
                Destroy(transform.gameObject);
                Destroy(explosion,1.0f);
            }
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        Destroy(transform.gameObject,5.0f);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
