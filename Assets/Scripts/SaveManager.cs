﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DefaultNamespace;
using GoogleARCore;
using UnityEngine;

public class SaveManager : MonoBehaviour
{

    private string saveFile;

    public List<PersistanceObject> PersistanceObjects;
    public PersistantPrefabStruct[] SpawnStructs;
    public Dictionary<PersistanceObjectTypes,GameObject> spawnPrefab;
    private string debugText = "nimic";
    
    private void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.fontSize = 30;

        GUI.Label(new Rect(500, 10, 250, 500), debugText, style);
    }
    
    public void Save()
    {
        using (BinaryWriter strWri = new BinaryWriter(new FileStream(saveFile,FileMode.OpenOrCreate)))
        {
            strWri.Write(PersistanceObjects.Count);
            foreach (var p in PersistanceObjects)
            {
                Debug.Log("Saving "+p.name);
                p.Save(strWri);
            }
        }
        
    }

    public void Load()
    {
        using (BinaryReader strRea = new BinaryReader(new FileStream(saveFile,FileMode.Open)))
        {
            int count = strRea.ReadInt32();
            Debug.Log(count);
            for(int i=0; i<count; ++i)
            {
                this.Load(strRea);
            }
        }
        
    }
    
    /// <summary>
    /// Order ObjectType needAnchor Position Rotation HasPersistanceParent parent ( same order)
    /// </summary>
    /// <param name="strRea"></param>
    public GameObject Load(BinaryReader strRea)
    {
        Vector3 position = Vector3.zero;
        Quaternion rotation = Quaternion.identity;
        GameObject objectLoaded = null;
        
        try
        {
            PersistanceObjectTypes type = (PersistanceObjectTypes) Enum.Parse(typeof(PersistanceObjectTypes), strRea.ReadString());
            Vector3[] vertices = null;
            if (type == PersistanceObjectTypes.Road)
            {
                vertices = RoadLoad(strRea);
            }
            bool needAnchor = strRea.ReadBoolean();
            position = new Vector3(strRea.ReadSingle(),strRea.ReadSingle(),strRea.ReadSingle());
            rotation = new Quaternion(strRea.ReadSingle(),strRea.ReadSingle(),strRea.ReadSingle(),strRea.ReadSingle());
            bool haveParent = strRea.ReadBoolean();
            objectLoaded = Instantiate(this.spawnPrefab[type], position, rotation);

            if (vertices != null)
            {
                RoadPiece roadPiece = objectLoaded.GetComponent<RoadPiece>();
                //roadPiece.vertices = vertices.ToList();
                roadPiece.centerPoints = vertices.ToList();
                roadPiece.GenerateStraightMesh();
            }
            
            if (needAnchor)
            {
                StartCoroutine(CreateAnchor(objectLoaded));
                
            }
            else if (haveParent)
            {
                objectLoaded.transform.SetParent(Load(strRea).transform);
            }
            this.PersistanceObjects.Add(objectLoaded.GetComponent<PersistanceObject>());

            //debugText += "\n" + position + rotation;
            
            
            Debug.Log("Finished load");
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return null;
        }

        return objectLoaded;
    }

    private Vector3[] RoadLoad(BinaryReader strRea)
    {
        int vLen = strRea.ReadInt32();
        debugText = vLen + " puncte";
        Vector3[] vertices = new Vector3[vLen];
        for (int i = 0; i < vLen; ++i)
        {
            vertices[i] = new Vector3(strRea.ReadSingle(),strRea.ReadSingle(),strRea.ReadSingle());
        }

        return vertices;
    }

    IEnumerator CreateAnchor(GameObject loadedObject)
    {
        float waitTime = 1;
        int i = 0;
        Anchor anchor = null;
        do
        {
            TrackableHit hit;
            TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon;

            if (Frame.Raycast(Screen.width / 2, Screen.height / 2, raycastFilter, out hit))
            {
                anchor = hit.Trackable.CreateAnchor(hit.Pose);
                loadedObject.transform.SetParent(anchor.transform,true);
            }

            i++;
            yield return new WaitForSeconds(waitTime);

        } while (anchor==null && i<150);
    }

    private void Awake()
    {
        spawnPrefab = new Dictionary<PersistanceObjectTypes, GameObject>();
        foreach (var ss in SpawnStructs)
        {
            spawnPrefab.Add(ss.index,ss.prefab);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        saveFile = Path.Combine(Application.persistentDataPath, "savefile.dat");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
