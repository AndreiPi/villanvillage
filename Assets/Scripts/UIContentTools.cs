﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIContentTools : MonoBehaviour
{

    public float SeparatorLen = 10.0f;
    private void SetChildrenAnchors()
    {
        RectTransform[] children = GetComponentsInChildren<RectTransform>();
        RectTransform parent = GetComponent<RectTransform>();
        Debug.Log("C "+parent.name + " "+parent.rect.width + " "+ parent.rect.height);
        int directChild = 0;
        float childYPos = 0;
        int totalChildren = 0;
        float parentHeight = 0;
        for (int i = 1; i < children.Length; ++i)
        {
            if (children[i].parent == transform)
            {
                totalChildren++;
                parentHeight += children[i].rect.height + SeparatorLen*2;
            }
        }

        parent.sizeDelta = new Vector2(parent.rect.width,parentHeight);
        
        for (int i = 1; i < children.Length; ++i)
        {
            if (children[i].parent == transform)
            {
                childYPos += children[i].rect.height + SeparatorLen;

                float YposRatio = Mathf.InverseLerp(-parent.rect.height / 2, parent.rect.height / 2, childYPos - parent.rect.height/2);
                float yPos = children[i].anchoredPosition.y;
                
                Debug.Log("C "+children[i].name + " "+children[i].rect.width + " "+ children[i].rect.height + " " + YposRatio + " "+childYPos);
                float w, h;
                w = children[i].rect.width / parent.rect.width / 2.0f;
                h = children[i].rect.height / parent.rect.height / 2.0f;
                children[i].anchorMin = new Vector2(0.5f - w,YposRatio -h);

                children[i].anchorMax = new Vector2(0.5f+w,YposRatio+h);
                
                children[i].offsetMin = new Vector2(0,0);
                children[i].offsetMax= new Vector2(0,0);
                
                
            }
           

            //children[i].position = new Vector3(parent.position.x, i * (children[i].rect.height + 20), 0);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        SetChildrenAnchors();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
