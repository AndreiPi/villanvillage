﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class RoadPiece : MonoBehaviour
{
    private Mesh mesh;
    public List<Vector3> vertices;
    public List<int> triangles;
    public List<Vector2> uv;
    public List<Color> colors;
    public List<Vector3> centerPoints;
    private bool generated;
    public float materialTileSize = 1;
    public float distance = 0.1f;

    int xSize;
    int ySize;

    private void Awake()
    {
    }

    public void GenerateMesh()
    {
        //GenerateTriangles();
        ScaleMaterial();
    }

    public void GenerateStraightMesh()
    {
        float xmin, xmax, ymin, ymax;
        int xminIndex, xmaxIndex, yminIndex, ymaxIndex;
        xminIndex = xmaxIndex = ymaxIndex = yminIndex = 0;
        xmax = ymax = float.MinValue;
        xmin = ymin = float.MaxValue;
        for (int i = 0; i < centerPoints.Count; i++)
        {
            if (centerPoints[i].x > xmax)
            {
                xmax = centerPoints[i].x;
                xmaxIndex = i;
            }

            if (centerPoints[i].x < xmin)
            {
                xmin = centerPoints[i].x;
                xminIndex = i;
            }

            if (centerPoints[i].z > ymax)
            {
                ymax = centerPoints[i].z;
                ymaxIndex = i;
            }

            if (centerPoints[i].z < ymin)
            {
                ymin = centerPoints[i].z;
                yminIndex = i;
            }
        }
        
        

        xSize = ySize = 8;
        if (xmax - xmin < ymax - ymin)    
        {
            ySize = (int) (Vector3.Distance(centerPoints[ymaxIndex],centerPoints[yminIndex])/distance);
            //ySize = centerPoints.Count-1;
            //distance = Vector3.Distance(centerPoints[ymaxIndex], centerPoints[yminIndex]) / ySize;
            CreateQuad(0,centerPoints[0], xSize,ySize);
            float orientation = ymaxIndex > yminIndex ? 1 : -1;
            float angleDirection = (centerPoints[ymaxIndex].x > centerPoints[yminIndex].x ? 1 : -1) * orientation;
            //Debug.Log(orientation + "  " + angleDirection + "   "+ centerPoints[ymaxIndex].x +"   "+ centerPoints[yminIndex].x);
            Vector3 drawVector = centerPoints[ymaxIndex] - centerPoints[yminIndex] * orientation;
            float angle = Vector3.Angle(drawVector, Vector3.forward) *angleDirection;
            transform.Rotate(Vector3.up,angle);
            //ModifyQuad(xSize,ySize,false);
        }
        else
        {
            xSize = (int) (Vector3.Distance(centerPoints[xmaxIndex],centerPoints[xminIndex])/distance);
           //xSize = centerPoints.Count-1;
           // distance = Vector3.Distance(centerPoints[xmaxIndex], centerPoints[xminIndex]) / xSize;
            CreateQuad(0,centerPoints[0], xSize ,ySize);
            float orientation = xmaxIndex > xminIndex ? 1 : -1;
            float angleDirection = (centerPoints[xmaxIndex].z < centerPoints[xminIndex].z ? 1 : -1) * orientation;
            Vector3 drawVector = centerPoints[xmaxIndex] - centerPoints[xminIndex] * orientation;
            float angle = Vector3.Angle(drawVector, Vector3.right) *angleDirection;
            transform.Rotate(Vector3.up,angle);
            //ModifyQuad(xSize,ySize,true);
        }
        
        
        
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "Procedural Mesh";
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.uv = uv.ToArray();
        mesh.colors = colors.ToArray();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
        MeshCollider meshCollider = this.gameObject.AddComponent<MeshCollider>();
        meshCollider.sharedMesh = mesh;
        
    }

    private void ScaleMaterial()
    {
        Material material = GetComponent<MeshRenderer>().material;
        material.SetTextureScale("_MainTex",
            new Vector2(xSize * materialTileSize, ySize * materialTileSize));
        material.SetTextureScale("_MainBump",
            new Vector2(xSize * materialTileSize, ySize * materialTileSize));
        material.SetTextureScale("_FadeTex",
            new Vector2(xSize * materialTileSize, ySize * materialTileSize));
    }

    private void CreateQuad(int verticesIndex, Vector3 center, int width, int height)
    {
        width = width % 2 == 1 ? ++width : width;
        height = height % 2 == 1 ? ++height : height;
        
        for(int i=0; i<=width; ++i)
        for (int j = 0; j <= height; ++j)
        {
            vertices.Add(
                new Vector3(center.x + distance*(i), center.y, center.z +distance*(j) )); 
            uv.Add(new Vector2(i,j));

            if (width < height)
            {
                colors.Add(new Color(1, 1, 1, 
                    Mathf.Clamp(1-Mathf.Abs(i-width/2)/((float)width/2)*1.5f,0,1 )));
                //Debug.Log(Mathf.Abs(i-width/2)/(float)width/2);
            }
            else
            {
                colors.Add(new Color(1, 1, 1,
                    Mathf.Clamp(1- Mathf.Abs(j-height/2)/((float)height/2),0,1) ));
                //Debug.Log(Mathf.Abs(j-height/2) + " " +(float)height/2 + "= " + Mathf.Abs(j-height/2)/((float)height/2) );
            }

        }

        int p1, p2, p3, p4;
        for (int i = 0; i<width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                p1 = verticesIndex + i * (height+1) +j;
                p2 = verticesIndex + (i+1) * (height + 1) + (j + 1);
                p3 = verticesIndex + (i+1) * (height + 1) + j;
                p4 = verticesIndex + i * (height + 1) + (j+1);
                triangles.Add(p1);
                triangles.Add(p2);
                triangles.Add(p3);
                
                triangles.Add(p1);
                triangles.Add(p4);
                triangles.Add(p2);
            }
        }
        
        
        
    }

    private void ModifyQuad(int width, int height, bool ModifyWidth)
    {
        //modify y
        if (ModifyWidth)
        {
            for (int i = 0; i <= width; i ++)
            {
                for (int j = 0; j <= height; ++j)
                {
                    vertices[i * (height + 1) + j] += Vector3.forward *  centerPoints[i].z;
                }
            } 
        }
        else
        {
            for (int i = 0; i <= width; i ++)
            {
                for (int j = 0; j <= height; ++j)
                {
                    vertices[i * (height + 1) + j] += Vector3.right *  centerPoints[j].x;
                }
            } 
        }
        
        
        
       
    }

    // Start is called before the first frame update
    void Start()
    {



    }

    // Update is called once per frame
    void Update()
    {
    }
}