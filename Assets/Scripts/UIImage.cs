﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIImage : MonoBehaviour,IPointerClickHandler
{
    public RoadGenerator roadGenerator;
    public SpawnObject spawnPrefab;
    private Text text;
    
    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        roadGenerator.choosenPrefab = spawnPrefab;
        Debug.Log("We chose " +roadGenerator.choosenPrefab);
        text.color = Color.red;
        
    }
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponentInChildren<Text>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (roadGenerator.choosenPrefab != spawnPrefab)
        {
            text.color = Color.blue;
        }
    }
}
